import json

import requests

headers = {'Content-type': 'application/json; charset=UTF-8'}
user = {"user": {
    "username": "reilgan7",
    "email": "reilgan7@user.user",
    "password": "12345678"
    }
}


def create_user():
    return requests.post('http://localhost:9998/api/users/', data=json.dumps(user, indent=4), headers=headers)


def login():
    return requests.post('http://localhost:9998/api/users/login/', data=json.dumps(user, indent=4), headers=headers)


def message_red_stat(token):
    body = {
        "token": token,
        "message_body_type": "MessageAmountRed",
        "message_body": {"amount_red": 45.7,
                         "image_id": 1,
                         "image_name": "image.jpg"},
    }

    return requests.post('http://localhost:9999/api/messages/', data=json.dumps(body, indent=4), headers=headers)


def subscribers(token):
    body = {
        "token": token,
        "rules": [{'name': 'RuleAmountRedGreaterThanValue', 'value': 13.1}]
    }
    return requests.post('http://localhost:9999/api/subscribers/', data=json.dumps(body, indent=4), headers=headers)


if __name__ == '__main__':
    print(create_user())
    response = json.loads(login().text)
    print(response)
    token = response['user']['token']
    print(message_red_stat(token))
    print(subscribers(token))