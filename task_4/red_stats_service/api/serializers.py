import jwt
from rest_framework import serializers

from .models import MESSAGES_BODY_TYPES


class MessegeRedStatSerializer(serializers.Serializer):
    message_body_type = serializers.CharField(max_length=255, allow_null=False)
    message_body = serializers.JSONField(allow_null=False)
    token = serializers.CharField(allow_null=False)

    def validate(self, data):
        token = data.get('token')
        message_body_type = data.get('message_body_type')
        message_body = data.get('message_body')

        if message_body is None:
            raise serializers.ValidationError(
                'Not specified message_body'
            )

        amount_red = message_body.get('amount_red')

        if amount_red is None:
            raise serializers.ValidationError(
                'Not specified amount_red'
            )

        if message_body_type is None:
            raise serializers.ValidationError(
                'Not specified message_body_type'
            )

        if message_body_type not in MESSAGES_BODY_TYPES.keys():
            raise serializers.ValidationError(
                'Unknown message_body_type'
            )

        return {'message_body_type': message_body_type,
                'message_body': message_body,
                'token': token}


class SubscribersSerializer(serializers.Serializer):
    token = serializers.CharField(allow_null=False)
    rules = serializers.JSONField(allow_null=False)

    def validate(self, data):
        token = data.get('token')
        rules = data.get('rules')

        if token is None:
            raise serializers.ValidationError(
                'Not specified token'
            )

        if rules is None:
            raise serializers.ValidationError(
                'Not specified rules'
            )

        return {'token': token,
                'rules': rules}

