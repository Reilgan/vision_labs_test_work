from django.urls import path

from .views import MessegeRedStatAPI, SubscribersAPI

app_name = 'api'
urlpatterns = [
    path('messages/', MessegeRedStatAPI.as_view()),
    path('subscribers/', SubscribersAPI.as_view())
]