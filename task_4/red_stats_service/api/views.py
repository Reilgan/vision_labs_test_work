import jwt
from django.conf import settings
from rest_framework import status, serializers
from rest_framework.response import Response
from rest_framework.views import APIView

from .serializers import MessegeRedStatSerializer, SubscribersSerializer
from .models import MESSAGES_BODY_TYPES, Messages, MessageAmountRed, Subscribers, SUBSCRIBERS_RULE_TYPES, \
    RuleAmountRedGreaterThanValue
from django.db import transaction, IntegrityError


def decode_token(token):
    decode = jwt.decode(token, settings.PUBLIC_JWT_KEYS, algorithms=["RS256"])
    return decode.get('id'), decode.get('username'), decode.get('email')


class MessegeRedStatAPI(APIView):
    """
     EndPoint для сообщений от red_stats
    """
    serializer_class = MessegeRedStatSerializer

    def post(self, request):
        serializer = self.serializer_class(data=request.data)
        serializer.is_valid(raise_exception=True)
        try:
            user_id, username, email = decode_token(serializer.data['token'])
        except Exception:
            return Response({}, status=status.HTTP_401_UNAUTHORIZED)

        if user_id is None or username is None or email is None:
            raise serializers.ValidationError(
                'not valid token'
            )

        with transaction.atomic():
            message = Messages(user_id=user_id,
                               username=username,
                               email=email,
                               message_body_type=serializer.data['message_body_type'],
                               message_body_id=None)
            message.save()

            if MESSAGES_BODY_TYPES[serializer.data['message_body_type']] is MessageAmountRed:
                message_body = MessageAmountRed(message=message,
                                                amount_red=serializer.data['message_body']['amount_red'],
                                                image_id=serializer.data['message_body']['image_id'],
                                                image_name=serializer.data['message_body']['image_name'],
                                                )
            else:
                return Response({'Error: not find message_body_type'}, status=status.HTTP_500_INTERNAL_SERVER_ERROR)
            message_body.save()
            message.message_body_id = message_body.pk
            message.save()

        return Response(status=status.HTTP_204_NO_CONTENT)


class SubscribersAPI(APIView):
    """
    EndPoint обслуживающий подписку на сообщения
    """
    serializer_class = SubscribersSerializer

    def post(self, request):
        serializer = self.serializer_class(data=request.data)
        serializer.is_valid(raise_exception=True)
        try:
            user_id, username, email = decode_token(serializer.data['token'])
        except Exception:
            return Response({}, status=status.HTTP_401_UNAUTHORIZED)

        rules = serializer.data['rules']

        with transaction.atomic():
            subscriber = Subscribers(user_id=user_id,
                                     username=username,
                                     email=email)
            try:
                subscriber.save()
            except IntegrityError:
                return Response(status=status.HTTP_400_BAD_REQUEST)

            rules_dict = []
            for rule in rules:
                if SUBSCRIBERS_RULE_TYPES[rule['name']] is RuleAmountRedGreaterThanValue:
                    rule_obj = RuleAmountRedGreaterThanValue(subscriber=subscriber,
                                                             value=rule['value'])

                    rule_obj.save()
                    rules_dict.append((rule['name'], rule_obj.pk))
                else:
                    return Response({'Error: not find SUBSCRIBERS_RULE_TYPES'},
                                    status=status.HTTP_500_INTERNAL_SERVER_ERROR)

            subscriber.rules = rules_dict
            subscriber.save()

        for rule in subscriber.rules:
            if SUBSCRIBERS_RULE_TYPES[rule[0]] is RuleAmountRedGreaterThanValue:
                rule = RuleAmountRedGreaterThanValue.objects.get(id=rule[1])
                rule.run()

        return Response(status=status.HTTP_204_NO_CONTENT)







