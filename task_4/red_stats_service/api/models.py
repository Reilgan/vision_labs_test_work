from django.db import models


class Messages(models.Model):
    user_id = models.IntegerField()
    username = models.CharField(max_length=255)
    email = models.EmailField()
    timestamp = models.DateTimeField(auto_now_add=True)
    message_body_type = models.CharField(max_length=255)
    message_body_id = models.IntegerField(null=True)
    is_sent = models.BooleanField(default=False)

    class Meta:
        ordering = ["-timestamp"]


class MessageAmountRed(models.Model):
    message = models.ForeignKey(Messages, on_delete=models.CASCADE)
    amount_red = models.FloatField()
    image_id = models.IntegerField()
    image_name = models.CharField(max_length=255)


class Subscribers(models.Model):
    user_id = models.IntegerField(unique=True, null=False)
    username = models.CharField(unique=True, max_length=255)
    email = models.EmailField(unique=True)
    rules = models.JSONField(null=True)
    is_active = models.BooleanField(default=True)


class RuleAmountRedGreaterThanValue(models.Model):
    """
    Модель обслуживающая правило: Картинка, у которой красного больше, чем заданное клиентом значение
    """
    subscriber = models.ForeignKey(Subscribers, on_delete=models.CASCADE)
    value = models.FloatField()

    def run(self):
        messages = Messages.objects.filter(user_id=self.subscriber.user_id).exclude(is_sent=True)
        for message in messages:
            message_body = MessageAmountRed.objects.filter(message=message)
            if message_body[0].amount_red > self.value:
                #TODO логика отправки сообщений клиенту?
                message.is_sent = True
                message.save()


MESSAGES_BODY_TYPES = {'MessageAmountRed': MessageAmountRed}
SUBSCRIBERS_RULE_TYPES = {'RuleAmountRedGreaterThanValue': RuleAmountRedGreaterThanValue}


