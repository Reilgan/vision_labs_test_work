import time
import runtimecheck


@runtimecheck.benchmark
def func_1():
    time.sleep(1)


@runtimecheck.benchmark
def func_2():
    time.sleep(1)


@runtimecheck.benchmark
def func_3():
    time.sleep(3)


@runtimecheck.benchmark
def func_4():
    time.sleep(4)


if __name__ == '__main__':
    func_1()
    func_2()
    func_3()
    func_4()
