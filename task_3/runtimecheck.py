import time
import logging

logger = logging.getLogger('runtime check')


def benchmark(func):
    def wrapper(*args, **kwargs):
        t = time.time()
        res = func(*args, **kwargs)
        logger.warning(f'{func.__name__}: {time.time() - t}')
        return res
    return wrapper