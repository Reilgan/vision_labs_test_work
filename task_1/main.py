import json
from decimal import Decimal


def compare_json(obj_1, obj_2, r=False) -> bool:
    if r:
        json1 = obj_1
        json2 = obj_2
    else:
        json1 = json.loads(obj_1)
        json2 = json.loads(obj_2)

    result = True

    try:
        for k in json2.keys():
            v_1 = json1[k]
            v_2 = json2[k]
            if isinstance(v_1, float):
                result = Decimal(v_1).quantize(Decimal('0.00001')) == Decimal(v_2).quantize(Decimal('0.00001'))
            elif isinstance(v_1, dict):
                result = compare_json(v_1, v_2, r=True)
            else:
                result = v_1 == v_2

    except Exception:
        return False

    return result


def test_1():
    dict_1 = {'1': 'test_1',
              '2': 'test_2',
              '3': 0.0001,
              '4': {'1': 'test_1',
                    '2': 'test_2',
                    '3': 0.0001}}
    dict_2 = {'1': 'test_1',
              '2': 'test_2',
              '3': 0.0001,
              '4': {'1': 'test_1',
                    '2': 'test_2',
                    '3': 0.0001}
              }

    json_1 = json.dumps(dict_1, indent=4)
    json_2 = json.dumps(dict_2, indent=4)

    return compare_json(json_1, json_2)

def test_2():
    dict_1 = {'q': 'test_1',
              '2': 'test_2',
              '3': 0.0001,
              '4': {'1': 'test_1',
                    '2': 'test_2',
                    '3': 0.0001}}
    dict_2 = {'1': 'test_1',
              '2': 'test_2',
              '3': 0.0001,
              '4': {'1': 'test_1',
                    '2': 'test_2',
                    '3': 0.0001}
              }

    json_1 = json.dumps(dict_1, indent=4)
    json_2 = json.dumps(dict_2, indent=4)

    return compare_json(json_1, json_2)

def test_3():
    dict_1 = {'1': 'test_1',
              '2': 'test_2',
              '3': 0.0001,
              '4': {'1': 'test_1',
                    '2': 'test_2',
                    '3': 5.12345}}
    dict_2 = {'1': 'test_1',
              '2': 'test_2',
              '3': 0.0001,
              '4': {'1': 'test_1',
                    '2': 'test_2',
                    '3': 0.0001}
              }

    json_1 = json.dumps(dict_1, indent=4)
    json_2 = json.dumps(dict_2, indent=4)

    return compare_json(json_1, json_2)


if __name__ == '__main__':
    print('t1:', test_1())
    print('t2:', test_2())
    print('t3:', test_3())