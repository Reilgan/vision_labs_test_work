import argparse
import requests
import os

host = 'http://localhost'
image_extension = (".jpg", ".png", ".gif", ".jpeg", ".JPG", ".PNG", ".GIF", ".JPEG")


def load_images_from_folder(folder):
    imgs = []
    for fn in os.listdir(folder):
        filename, file_extension = os.path.splitext(fn)
        if file_extension in image_extension:
            img = open(filename, 'rb')
            if img is not None:
                imgs.append(img)
    return imgs


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument("--path")
    args = parser.parse_args()
    path = args.path

    if path:
        images = load_images_from_folder(path)
        files = {'upload_file': images}

        x = requests.post(host + '/images', files=files)
        print(x)
    else:
        print('Folder path not specified, args: --path')

